<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Claymore's Miner Monitor</title>

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<style type="text/css">
			body { 
				padding-top: 50px; 
			}
			.label-hashrate {
				padding-top: 8px;
			}
			.hashrate {
				margin-bottom: 20px;
			}
			.table {
				border-radius: 4px;
			}
			.hidden {
				visibility: hidden;
			}
		</style>
	</head>
	<body>

		<nav class="navbar navbar-default  navbar-fixed-top"> 
			<div class="container-fluid"> 
				<div class="navbar-header"> 
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
	      			<a class="navbar-brand" href="guacal.php">Claymore's Miner Monitor</a>
				</div>

			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li><a id="settings-button" href="#">Settings</a></li> 
			      </ul>
			    </div>
			</div> 
		</nav>

		<div id="main" class="container">
			<?php 
			$service_port = "3333";
			$address = "25.15.126.14";
			$conex = true;
			$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
			if ($socket === false) {
			    echo "socket_create() falló: razón: " . socket_strerror(socket_last_error());
			}else{
				$conex = false;
			}

			$result = socket_connect($socket, $address, $service_port);
			if ($result === false) {
			    echo "socket_connect() falló.Razón: ($result) " . socket_strerror(socket_last_error($socket));
			}else{
				$conex = false;
			}

			$in = '{"id":0,"jsonrpc":"2.0","method":"miner_getstat1"}';
			socket_write($socket, $in, strlen($in));

			$h = 1;
			while ($out = socket_read($socket, 2048)) {
				$json = json_decode($out, true);
				$mh = split(";", $json["result"][2]);
				$temps = split(";", $json["result"][6]); ?>
				<h2>Rig <?= $h++ ?> guacal</h2>
				<h2 class="hashrate">Hashrate <span class="label label-success label-hashrate"><?= number_format($mh[0]) ?></span></h2>
				<div class="table-responsive">
					<table class="table table-bordered table-striped"> 
						<thead>
							<tr>
								<th>GPU</th>
								<th>Temp</th>
								<th>Fan</th>
							</tr>
						</thead>
						<tbody>
							<?php $j=1; for ($i=0; $i < 12; $i=$i+2) { ?>
								<tr>
									<th><?= $j ?></th>
									<td><?= $temps[$i] ?>°</td>
									<td><?= $temps[$i+1] ?>%</td>
								</tr>
							<?php $j++; } ?>
						</tbody>
					</table> 
				</div>
				<hr>
			<?php }
			socket_close($socket); ?>
			<footer>The autoreload is set every <cite title="Source Title">60 seconds</cite></footer>
			
			<?php
				if($mh < 160){
					$to      = 'robles.juanc@gmail.com';
					$title    = 'Guacal tiene problemas';
					$message    = 'Guacal tiene problemas';
					if($mh < 160){
						$title    = 'Guacal tiene problemas de producción';
						$message   = 'Guacal tiene problemas esta produciendo '.$mh.' mh/s';
					}elseif(!$conex){
						$title    = 'Guacal tiene problemas de conexión';
						$message   = 'Guacal tiene problemas no hay conexion con el rig';
					}
					$headers = 'From: server@guacal' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();
					mail($to, $title, $message, $headers);
				}
			?>
		</div>
		<div id="settings" class="hidden container">
			<h2>Settings</h2>
			<form>
			  <div class="form-group">
			    <label for="ip">Rig IP</label>
			    <input type="text" class="form-control" id="ip">
			  </div>
			  <div class="form-group">
			    <label for="hashrate">Target Hashrate</label>
			    <input type="text" class="form-control" id="hashrate">
			  </div>
			  <div class="form-group">
			    <label for="time">Reload time</label>
			    <input type="text" class="form-control" id="time">
			  </div>
			  <div class="form-group">
			    <label for="email">Notification email</label>
			    <input type="email" class="form-control" id="email">
			  </div>
			  <button type="submit" class="btn btn-default">Save</button>
			</form>
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

		<script>
			$(function() {
				$("#settings-button").click(function() {
					$("#settings").removeClass("hidden");
					$("#main").addClass("hidden");
					$("#bs-example-navbar-collapse-1").removeClass("collapse in");
					$("#bs-example-navbar-collapse-1").addClass("collapse");
				});
			});
			setTimeout(function () { location.reload(1); }, 60000);
		</script>
	</body>
</html>